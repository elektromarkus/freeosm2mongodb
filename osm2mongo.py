# -*- coding: utf-8 -*-

import osmium
from pymongo import MongoClient, GEO2D, ASCENDING
import datetime


class Stopwatch:
    def __init__(self):
        self.starttime = datetime.datetime.now()
        self.stoptime = self.starttime
        self.laptime = self.starttime
        self.lapcounter = 0

    def lap(self):
        now = datetime.datetime.now()
        delta = now - self.laptime
        self.laptime = now
        return int(delta.total_seconds() * 1000)

    def stop(self):
        now = datetime.datetime.now()
        # lapdelta = now - self.laptime
        totaldelta = now - self.starttime
        self.laptime = now
        return int(totaldelta.total_seconds() * 1000)

    def start(self):
        self.starttime = datetime.datetime.now()
        self.stoptime = self.starttime
        self.laptime = self.starttime
        self.lapcounter = 0


class CounterHandler(osmium.SimpleHandler):
    def __init__(self, client,
                 dbname="xgeo",
                 nodebuffersize: object = 100000,
                 waybuffersize=10000,
                 relationbuffersize=1000,
                 truerun=True):

        osmium.SimpleHandler.__init__(self)

        self.truerun = truerun
        self.nodebuffersize = nodebuffersize
        self.waybuffersize = waybuffersize
        self.relationbuffersize = relationbuffersize

        self.nodebuffer = [{} for i in range(self.nodebuffersize)]
        self.waybuffer = [{} for i in range(self.waybuffersize)]
        self.relationbuffer = [{} for i in range(self.relationbuffersize)]

        self.num_nodes = 0
        self.num_ways = 0
        self.num_relations = 0
        self.client = client
        # self.geodb = self.client.geo
        # print(dbname)

        if self.truerun:
            self.geodb = self.client[dbname]
            # print("Dropping DB: {}".format(dbname))
            self.geodb.nodes.drop()
            self.geodb.ways.drop()
            self.geodb.relations.drop()

            # print("Create indexes")
            self.geodb.nodes.create_index([("loc", GEO2D)])
            self.geodb.nodes.create_index([("id", ASCENDING)])

            self.geodb.ways.create_index([("id", ASCENDING)])
            self.geodb.ways.create_index([("tags.ref", ASCENDING)])
            self.geodb.ways.create_index([("tags.highway", ASCENDING)])
            self.geodb.ways.create_index([("nodes", ASCENDING)])

            self.geodb.relations.create_index([("id", ASCENDING)])

        self.nsw = Stopwatch()

    def printNode(self, n):
        print("{}\t{} lat:{} lon:{}".format(self.num_nodes, n.id, n.location.lat, n.location.lon))
        print(type(n.tags))
        tags = [{'k': tag.k, 'v': tag.v} for tag in n.tags]
        print(type(tags))
        print("\t{}".format(n.tags))
        for t in tags:
            print(type(t))

    def node(self, n):
        newnode = {'id': n.id,
                   'loc': [n.location.lat, n.location.lon],
                   'tags': self.tags2tags(n.tags),
                   }
        self.nodebuffer[self.num_nodes % self.nodebuffersize] = newnode
        self.num_nodes += 1

        if not self.num_nodes % self.nodebuffersize:
            print("\rWrite Node Buffer     Lap:{:5d} msec of {:12d}    ".format(self.nsw.lap(), self.num_nodes), end="")
            if self.truerun:
                result = self.geodb.nodes.insert_many(self.nodebuffer)

    def printWay(self, w):
        print("{}\t{}".format(self.num_ways, w.nodes))
        print("\t{}".format(w.tags))

    def tags2tags(self, tags):
        result = {t.k.replace(":", "_D_").replace(".", "_P_"): t.v for t in tags}
        return result

    def way(self, w):
        newway = {'id': w.id,
                  'nodes': [n.ref for n in w.nodes],
                  'tags': self.tags2tags(w.tags),
                  }
        self.waybuffer[self.num_ways % self.waybuffersize] = newway
        self.num_ways += 1

        if not self.num_ways % self.waybuffersize:

            print("\rWrite Way Buffer      Lap:{:5d} msec of {:12d}    ".format(self.nsw.lap(), self.num_ways), end="")
            if self.truerun:
                result = self.geodb.ways.insert_many(self.waybuffer)

    def relation(self, r):
        newrelation = {
            'id': r.id,
            'tags': self.tags2tags(r.tags),
            'members': [{'ref': m.ref, 'type': m.type, 'role': m.role} for m in r.members]
        }
        self.relationbuffer[self.num_relations % self.relationbuffersize] = newrelation
        self.num_relations += 1

        if not self.num_relations % self.relationbuffersize:
            print("\rWrite Relation Buffer Lap:{:5d} msec of {:12d}    ".format(self.nsw.lap(), self.num_relations),
                  end="")
            if self.truerun:
                result = self.geodb.relations.insert_many(self.relationbuffer)

    def flushBuffer(self):
        if self.truerun:
            print("Flush Buffer with elemnts")
            print(self.num_relations % self.relationbuffersize)
            print(self.num_ways % self.waybuffersize)
            print(self.num_nodes % self.nodebuffersize)
            result = self.geodb.relations.insert_many(
                self.relationbuffer[0:self.num_relations % self.relationbuffersize])
            result = self.geodb.ways.insert_many(self.waybuffer[0:self.num_ways % self.waybuffersize])
            result = self.geodb.nodes.insert_many(self.nodebuffer[0:self.num_nodes % self.nodebuffersize])

    def __del__(self):
        print("Delete Counterhandler")


def main():
    client = MongoClient("mongodb://localhost:27017")
    region = "isle-of-man-latest"

    h = CounterHandler(client, dbname=region, truerun=True)
    totalsw = Stopwatch()
    h.apply_file(region + ".osm.pbf")
    h.flushBuffer()

    print()
    print("Number of Nodes: %d" % h.num_nodes)
    print("Number of Relations: %d" % h.num_relations)
    print("Number of Ways: %d" % h.num_ways)
    print(totalsw.stop())


if __name__ == '__main__':
    # check()
    main()
